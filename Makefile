book-a4.pdf: book.md
	pandoc                           \
	    --standalone                 \
	    --toc                        \
	    --highlight-style zenburn    \
	    -V documentclass:extarticle  \
	    -V papersize:A4              \
	    -V fontsize:14pt             \
	    -V geometry:"top=2.5cm,      \
	                 bottom=2.6cm,   \
	                 left=2.5cm,     \
	                 right=2.5cm"    \
	    $< -o $@
	pdftk cover/cover-it-A4.pdf book-a4.pdf output final/mobile-phone-security-it-A4.pdf 

book-a5.pdf: book.md
	pandoc                           \
	    --standalone                 \
	    --toc                        \
	    --highlight-style zenburn    \
	    -V documentclass:extarticle  \
	    -V papersize:A5              \
	    -V fontsize:14pt             \
	    -V geometry:"top=2.5cm,      \
	                 bottom=2.6cm,   \
	                 left=2.5cm,     \
	                 right=2.5cm"    \
	    $< -o $@
	pdftk cover/cover-it-A5.pdf book-a5.pdf output final/mobile-phone-security-it-A5.pdf 


.PHONY: clean
clean:
	rm -f book-a5.pdf
	rm -f book-a5.ps

.PHONY: help
help:
	@echo "Usage:"
	@echo "  make TARGET"
	@echo
	@echo "Available targets:"
	@echo "  book-a5.pdf"
	@echo "  book-a5.pdf"
	@echo "  clean"
	@echo "  help"
